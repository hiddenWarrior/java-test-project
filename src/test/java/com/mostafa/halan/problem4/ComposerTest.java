package com.mostafa.halan.problem4;

import org.junit.*;
import static org.junit.Assert.*;
import java.util.function.Function;

import com.mostafa.halan.problem4.Composer;


public class ComposerTest {
    @Test
    public void testCompose()
    {
        Function<Integer, Integer> inc = (i) -> i + 1;
        Function<Integer, Integer> square = (i) -> i * i;
        Function<Integer,Integer> func = new Composer<Integer>().compose(square, inc);
        assertEquals(func.apply(6), Integer.valueOf(49));
    }

    
}
