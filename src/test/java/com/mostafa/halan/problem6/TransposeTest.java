package com.mostafa.halan.problem6;
import org.junit.*;
import static org.junit.Assert.*;


public class TransposeTest {
    @Test
    public void testMediumArray(){
        int [][] result = Transpose.transpose(new int[][]{new int[]{1,2},new int[]{3,4}});
        assertArrayEquals(result, new int[][]{new int[]{1,3},new int[]{2,4}});
    }    
    @Test
    public void testEmptyArray(){
        int [][] result = Transpose.transpose(new int[][]{});
        assertArrayEquals(result, new int[][]{});
    }    

    @Test
    public void testBigArray(){
        int [][] result = Transpose.transpose(new int[][]{new int[]{1,2,3,4}, new int[]{5,6,7,8}}); 
        assertArrayEquals(result, new int[][]{new int[]{1,5}, new int[]{2,6}, new int[]{3,7}, new int[]{4,8}});
    }    

}
