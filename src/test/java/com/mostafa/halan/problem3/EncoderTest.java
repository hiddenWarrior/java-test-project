package com.mostafa.halan.problem3;

import org.junit.*;
import static org.junit.Assert.*;


public class EncoderTest {
    Encoder encoder;
    
    public EncoderTest(){
        encoder = new Encoder();
    }
    
    @Test
    public void testEncoderSimple()
    {
        assertEquals(this.encoder.runLengthEncode("a"), "a1");
    }

    @Test
    public void testEncoderComplex()
    {
        assertEquals(this.encoder.runLengthEncode("aaaaaaaaaabbbaxxxxyyyzyx"), "a10b3a1x4y3z1y1x1");
    }

    @Test
    public void testEncoderEmpty()
    {
        assertEquals(this.encoder.runLengthEncode(""), "");
    }


    public void testDecoderGeneral(String value){
        assertEquals(this.encoder.runLengthDecode(encoder.runLengthEncode(value)), value);
    }
    
    @Test
    public void testDecoderComplex()
    {
        testDecoderGeneral("aaaaaaaaaabbbaxxxxyyyzyx");
    }

    @Test
    public void testDecoderSimple()
    {
        testDecoderGeneral("a");
    }

    @Test
    public void testDecoderEmpty()
    {
        testDecoderGeneral("");
    }

}
