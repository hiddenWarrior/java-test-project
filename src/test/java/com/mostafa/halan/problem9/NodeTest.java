package com.mostafa.halan.problem9;
import org.junit.*;
import static org.junit.Assert.*;


public class NodeTest {
    @Test
    public void testSingleNode(){
        Node node = new Node(17);
        assertEquals(node.getSum(), 17);
    }
    @Test
    public void testMultipleNodes(){
        Node [] nodes = {new Node(17), new Node(20), new Node(new Node[]{new Node(3),new Node(10)},0)};
        Node big = new Node(nodes, 50);
        assertEquals(big.getSum(), 100);
    }    

}
