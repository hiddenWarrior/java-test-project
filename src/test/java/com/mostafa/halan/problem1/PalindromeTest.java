package com.mostafa.halan.problem1;
import org.junit.*;
import static org.junit.Assert.*;


public class PalindromeTest {
    Palindrome palindrome;
    public PalindromeTest(){
        palindrome = new Palindrome();
    }
    private boolean test_palindrome(String s){
        return palindrome.isPalindrome(s);
    }
    @Test
    public void testPalanna()
    {
        assertTrue( this.test_palindrome("anna"));
    }

    @Test
    public void testPalapple()
    {
        assertFalse( this.test_palindrome("apple"));
    }
    
}
