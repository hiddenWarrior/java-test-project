package com.mostafa.halan.problem8;
import static org.junit.Assert.*;

import org.junit.Test;

public class FirstDuplicateTest {
    @Test
    public void TestDuplicatesNormal(){
        int [] arr = new int []{5, 17, 3, 17, 4, -1};
        assertEquals(3, FirstDuplicate.getFirstDuplicate(arr));
    }
    
    @Test
    public void TestDuplicatesUnique(){
        int [] arr = new int []{5, 17, 3, 4};
        assertEquals(FirstDuplicate.getFirstDuplicate(arr), -1);
    }

    @Test
    public void TestDuplicatesEmpty(){
        int [] arr = {};
        assertEquals(FirstDuplicate.getFirstDuplicate(arr), -1);
    }


}
