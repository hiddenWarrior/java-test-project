package com.mostafa.halan.problem5;
import org.junit.*;
import static org.junit.Assert.*;


public class UniqueTest {
    @Test
    public void testUnique()
    {
        String []a = new String[]{"apples", "oranges", "flowers", "apples"};
        String[] result = Unique.getUniqueValues(a);
        assertArrayEquals(result, new String[]{"oranges", "flowers"});
        
    }

}
