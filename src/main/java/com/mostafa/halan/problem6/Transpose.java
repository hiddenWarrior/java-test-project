package com.mostafa.halan.problem6;

public class Transpose {
    public static int [][] transpose(int [][] input){
        int [][] output = null;
        int height = input.length;
        if(height == 0){
            return new int[][]{};
        }
        int width = input[0].length;
        output = new int[width][height];
        for(int i = 0; i < height; i++)
            for(int j = 0; j < width ; j++){
                output[j][i] = input[i][j];
            }
        return output;
    }
    
}
