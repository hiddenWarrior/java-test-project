package com.mostafa.halan.problem8;
import java.util.Map;
import java.util.HashMap;

public class FirstDuplicate {

    public static int getFirstDuplicate(int [] arr){
        /*
         the time complexity is just iterating over the array so O(n) (all the operation in the loop is O(1))
         the space complexity is O(n) we just added entry uniquely for each item in the hashmap (space complexity of HashMap is O(n))
        */
        Map<Integer,Boolean> storage = new HashMap<Integer,Boolean>();
        for(int i=0;i < arr.length;i++){
            if(storage.getOrDefault(arr[i], false)){
                return i;
            }
            else{
                storage.put(arr[i], true);
            }
        }
        return -1;
    }
    
}
