package com.mostafa.halan.problem4;
import java.util.function.Function;

public class Composer<T> {
    public  Function<T,T> compose(Function<T,T> one, Function<T,T> two){
        return one.compose(two);
    }
    
}
