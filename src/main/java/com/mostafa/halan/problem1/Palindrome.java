package com.mostafa.halan.problem1;

public class Palindrome {
    public static boolean isPalindrome(String t){
        String reversedString = new StringBuilder(t).reverse().toString();
        return t.equals(reversedString);        
    }

}
