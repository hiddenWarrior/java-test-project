package com.mostafa.halan.problem3;

public class Encoder {
    
    private void checkUnloadBufferForEncode(StringBuilder sb, char buffer, int occurence){
        
        if(occurence > 0){
            sb.append(buffer);
            sb.append(Integer.toString(occurence));
        }

    }

    private void checkUnloadBufferForDecode(StringBuilder sb, char buffer, StringBuilder occurence){
        
        
            int repetition = Integer.parseInt(occurence.toString());
            for(int i = 0; i < repetition; i++){
                sb.append(buffer);
            }


    }

    public String runLengthEncode(String s){
        int occurence = 0;
        char chBuffer = 0;
        StringBuilder result = new StringBuilder();
        for (char c : s.toCharArray()) {
            if(chBuffer != c){
                checkUnloadBufferForEncode(result, chBuffer, occurence);
                occurence = 1;
                chBuffer = c;

            }
            else{
                occurence += 1;
            }
        }
        checkUnloadBufferForEncode(result, chBuffer, occurence);
        return result.toString();
    
    }


    public String runLengthDecode(String s){
        StringBuilder result = new StringBuilder();
        StringBuilder occurence = null;
        char chBuffer = 0;
        for (char c : s.toCharArray()) {
            if(Character.isLetter(c)){
                if(chBuffer != 0){
                    this.checkUnloadBufferForDecode(result, chBuffer, occurence);
                    occurence = null;
                    chBuffer = 0;
                }
                chBuffer = c;
                occurence = new StringBuilder();
            }
            else if(Character.isDigit(c)){
                occurence.append(c);
            }
        
        }
        if(chBuffer != 0){
            this.checkUnloadBufferForDecode(result, chBuffer, occurence);
        }

        return result.toString();


    }

}
