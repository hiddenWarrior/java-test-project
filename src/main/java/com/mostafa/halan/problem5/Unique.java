package com.mostafa.halan.problem5;
import java.util.HashMap;
import java.util.Map;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.function.Function;
import java.util.ArrayList;
import java.util.List;

public class Unique {
    public static String [] getUniqueValues(String [] repeatedValues){
        Map <String, Long> foundString = Arrays.stream(repeatedValues).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        return Arrays.stream(repeatedValues).filter(s -> foundString.get(s) < 2l ).toArray(String[]::new);
    }
    
}
