package com.mostafa.halan.problem9;

public class Node {
    private Node [] children;
    private int value;
    public Node(int value){
        this.value = value;
        children = null;

    }

    public Node(Node[] children,int value){
        this.value = value;
        this.children = children;
    }
    public int getSum(){
        int sum = value;
        if(children != null){
            for(Node child: children){
                sum += child.getSum();
            }
        }
        return sum;

    }
    
}
